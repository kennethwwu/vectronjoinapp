// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'starter.directives', 'ngCordova', 'ion-autocomplete', 'angular-md5'])

.run(function($ionicPlatform, AuthService, $rootScope, $localstorage, $cordovaDevice, Venues) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    $rootScope.device = {uuid:'12345', model:'1234', cordova:'1234', platform:'ios', version:'10.01'};
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);
      $rootScope.device = {uuid:$cordovaDevice.getUUID(), model:$cordovaDevice.getModel(), platform:$cordovaDevice.getPlatform(), version:$cordovaDevice.getVersion()};
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    if (window.MacAddress) {
      window.MacAddress.getMacAddress(function(macAddress) {

        $rootScope.device.MAC = macAddress;
        // console.log($rootScope.device);
      },function(fail) {console.log(fail);}
      );
    }

    // $rootScope.device = $cordovaDevice.getUUID();
    // console.log($rootScope.device);
  });

  //initial messages
  $rootScope.alerts = [];
  $rootScope.success = [];

  AuthService.useCredentials();
  $rootScope.isLogin = function(){
    return AuthService.isLogin();
  };

  $rootScope.getCurrentVenue = function(venue_id){
    Venues.getVenues().then(function(s){
      console.log(s);
      angular.forEach(s, function(v){
        if(v.id === venue_id){
          $rootScope.currentVenue = v;
        }
      });
    },function(e){
      console.log(e);
    });
  };

  if($rootScope.isLogin()){
    $rootScope.operator = $localstorage.get('OPERATOR')?$localstorage.get('OPERATOR'):'';
    $rootScope.venue_id = $localstorage.get('VENUE_ID')?parseInt($localstorage.get('VENUE_ID')):'';
    if($rootScope.venue_id){
      // $rootScope.currentVenue
      $rootScope.getCurrentVenue($rootScope.venue_id);
    }
  };
})

.constant('API_URL', 'https://api.vecport.net/vectron-crm/public/api')

.constant('APP_KEY', {
  product_key: 'a4168252-2617-4352-9976-d9320144774c',
  product_secret: 'a63b40f7a2f562db94cc9cc9e059acbb'
})

// .constant('$ionicLoadingConfig', {
//   template: '<ion-spinner ng-show="show" class="text-center"></ion-spinner>',
//   duration: 8000,
// })

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
  $stateProvider

  //   .state('app', {
  //   url: '/app',
  //   abstract: true,
  //   templateUrl: 'templates/menu.html',
  //   controller: 'AppCtrl'
  // })



  .state('newmember', {
    url: '/newmember',
    templateUrl: 'templates/newmember.html',
    controller: 'NewMemberCtrl'
    // views: {
    //   'menuContent': {
    //     templateUrl: 'templates/newmember.html',
    //     controller: 'NewMemberCtrl'
    //   }
    // },
    // resolve:{
    //   lists:function(Subscribers){
    //     return Subscribers.getLists().then(function(s){
    //       return s;
    //     },function(e){
    //       return {};
    //     });
    //   }
    // }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/newmember');

  //register the Interceptor
  $httpProvider.interceptors.push([
    '$injector',
    function ($injector) {
      return $injector.get('AuthInterceptor');
    }
  ]);

});
