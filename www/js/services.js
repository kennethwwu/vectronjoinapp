angular.module('starter.services', [])

//http response interceptor
.factory('AuthInterceptor', function ($rootScope, $q, API_URL) {
    return {
        responseError: function (response) {
            // /^http:\/\/52.62.21.183\/api\//.test(response.config.url)
            // console.log(response.config.url.match(api));
            if(response.config.url.match(/^https:\/\/api.vecport.net/) && (response.status == 401 || (response.status == 400 && response.data.error) || response.status == 403)){
                console.log(response.status);
                $rootScope.$broadcast('logout');

            }
            return $q.reject(response);
        }
    };
})

.factory('$localstorage', ['$window', function($window) {
    return {
        set: function(key, value) {
            $window.localStorage[key] = value;
        },
        get: function(key, defaultValue) {
            return $window.localStorage[key] || false;
        },
        setObject: function(key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function(key) {
            if($window.localStorage[key] != undefined)
                return JSON.parse($window.localStorage[key] || false );

            return false;
        },
        remove: function(key){
            $window.localStorage.removeItem(key);
        },
        clear: function(){
            $window.localStorage.clear();
        }
    }
}])

.service('AuthService', function($q, $http, $localstorage, APP_KEY, API_URL, md5){

    var isLogin = false;
    var accessToken ="";
    var operator = '';
    var secret = '';

    var useCredentials = function(){
        if($localstorage.get('ACCESS-TOKEN')){
            isLogin = true;
            accessToken = $localstorage.get('ACCESS-TOKEN');
            operator = $localstorage.get('OPERATOR');
            secret = $localstorage.get('SECRET');

        }
        else{
            isLogin = false;
            accessToken ="";
            operator = '';
            secret = '';
        }
    };

    var loginService = function(loginData, device){
        var d = $q.defer();
        var url = API_URL+'/signin2';
        data = {
            username:loginData.username,
            password:loginData.password,
            device:JSON.stringify(device),
            product_key:APP_KEY.product_key,
            product_secret:APP_KEY.product_secret
        };

        $http.post(url, data).then(
            function(s){
                console.log(s.data);
                d.resolve(s.data);
                $localstorage.set('ACCESS-TOKEN',s.data.token);
                $localstorage.set('SECRET', md5.createHash(loginData.password+'http://www.vectron.com.au/'));
                useCredentials();
        }, function(e){
            console.log(e);
                d.reject(e);
        }
        );

        return d.promise;
    };


    var logoutService = function(device){
        var d = $q.defer();
        var url = API_URL+'/operator/logout?token='+accessToken;
        var data = {
            device:JSON.stringify(device),
            product_key:APP_KEY.product_key,
            product_secret:APP_KEY.product_secret
        };
        $http.post(url, data).then(function(s){
            $localstorage.remove('ACCESS-TOKEN');
            $localstorage.remove('OPERATOR');
            $localstorage.remove('SECRET');
            useCredentials();
            d.resolve('logout');
        }, function(e){
          $localstorage.remove('ACCESS-TOKEN');
          $localstorage.remove('OPERATOR');
          $localstorage.remove('SECRET');
          useCredentials();
          d.reject(e);
        });
        return d.promise;
    };

    return{
        isLogin:function(){return isLogin;},
        accessToken:function(){return accessToken;},
        secret:function(){return secret;},
        operator:function(){return operator()},
        loginService:loginService,
        logoutService:logoutService,
        useCredentials:useCredentials,
    }

})

.service('User', function($q, $http, $localstorage){

    var user = {};

    var getUser = function(){
        var d = $q.defer();
        var url = API;

        return d.promise;
    };


    return{
        // isLogin:function(){return isLogin;},
        getUser:getUser,
    }

})


.service('Members', function($q, $http, APP_KEY, API_URL, AuthService){

    var addMember = function(member, venue_id, operator_name){
        if(!venue_id || !operator_name){
            return;
        }

        var d = $q.defer();
        var url = API_URL+'/operator/subscriber?token='+AuthService.accessToken();
        var data = {
            product_key:APP_KEY.product_key,
            product_secret:APP_KEY.product_secret,
            first_name:member.first_name,
            last_name:member.last_name,
            email:member.email,
            mobile:member.mobile,
            postcode:member.postcode,
            venue_id:venue_id,
            operator_name:operator_name,
        };

        $http.post(url, data).then(function(s){
            d.resolve(s);
        }, function(e){
            d.reject(e)
        });
        return d.promise;
    };

    return{
        addMember:addMember,
    }
})

.service('Venues', function($q, $http, APP_KEY, API_URL, AuthService){

    //get list names
    var getVenues = function(){
        var d = $q.defer();
        var url = API_URL+'/operator/venue?token='+AuthService.accessToken();
        var data = {
            product_key:APP_KEY.product_key,
            product_secret:APP_KEY.product_secret
        };
        $http.get(url, {params:data}).then(function(s){
            angular.forEach(s.data.data, function(venue){
              venue.setting.value = JSON.parse(venue.setting.value);
            });
            d.resolve(s.data.data);
        }, function(e){
            d.reject(e);
        });
        return d.promise;
    };


    return{
        getVenues:getVenues,
    }
});
